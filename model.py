import torch
import torch.nn as nn
import torchvision.transforms.functional as TF


class Conv3DBlock(nn.Module):
    """
    """

    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.conv1 = nn.Conv3d(in_channels=in_channels, out_channels=out_channels//2, kernel_size=(3,3,3), padding=1, bias=False)
        self.bn1 = nn.BatchNorm3d(num_features=out_channels//2)
        self.conv2 = nn.Conv3d(in_channels=out_channels//2, out_channels=out_channels, kernel_size=(3,3,3), padding=1, bias=False)
        self.bn2 = nn.BatchNorm3d(num_features=out_channels)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.relu(self.bn1(self.conv1(x)))
        x = self.relu(self.bn2(self.conv2(x)))
        return x




class Unet3D(nn.Module):
    def __init__(self, in_channels, out_channels, features):
        super().__init__()

        self.downsteps = nn.ModuleList()
        self.upsteps = nn.ModuleList()
        self.upconvs = nn.ModuleList()
        self.pool = nn.MaxPool3d(kernel_size=(2,2,2), stride=2)

        # Downward part of Unet
        for feature in features:
            self.downsteps.append(Conv3DBlock(in_channels=in_channels, out_channels=feature))
            in_channels = feature

        self.bottleneck = Conv3DBlock(features[-1], features[-1]*2)

        # Upward part of Unet
        for feature in features[::-1]:
            self.upsteps.append(nn.ConvTranspose3d(feature * 2, feature, kernel_size=(2,2,2), stride=2))
            self.upconvs.append(Conv3DBlock(feature*2, feature))

        self.final_conv = nn.Conv3d(features[0], out_channels, kernel_size=(1,1,1))


    def forward(self, x):
        skip_connections = []

        for downstep in self.downsteps:
            x = downstep(x)
            skip_connections.append(x)
            x = self.pool(x)

        x = self.bottleneck(x)

        for upstep, upconv, skip in zip(self.upsteps, self.upconvs, skip_connections[::-1]):
            x = upstep(x)
            concat = torch.cat((skip, x), dim=1)
            x = upconv(concat)

        x = self.final_conv(x)
        return x


def test():
    x = torch.randn((3, 1, 80, 80, 80))
    model = Unet3D(in_channels=1, out_channels=1, features=[64, 128, 256, 512])
    print(x.shape)
    pred = model(x)
    print(pred.shape)



if __name__ == "__main__":
    test()

