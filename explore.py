from pathlib import Path
from PIL import Image
import pydicom
from pydicom.filereader import read_dicomdir
from matplotlib import pyplot as plt
import numpy as np


def base_dir():
    return Path(__file__).parent.parent


def get_patient_dcm_dir(pid):
    dcm_dir = base_dir() / "Train_Sets" / "CT" / f"{pid}" / "DICOM_anon"
    assert dcm_dir.exists(), f"Patient dicom dir does not exist:\n{dcm_dir}"
    assert list(dcm_dir.rglob("*.dcm")), f"Patient dicom dir does not contain dicom files:\n{dcm_dir}"
    return dcm_dir


def get_patient_dcm(pid):
    dcm_dir = get_patient_dcm_dir(pid)
    dcm_set = []
    reverse = False
    for dcm_fp in dcm_dir.rglob("*.dcm"):
        dcm = pydicom.dcmread(dcm_dir / dcm_fp)
        assert dcm.PatientPosition in ["HFS", "FFS"], f"Unsupported patient orientation: {dcm.PatientPosition}"
        reverse |= dcm.PatientPosition == "HFS"
        dcm_set.append(dcm)
    # order based on slice location
    dcm_set.sort(key=lambda dcm: float(dcm.SliceLocation), reverse=reverse)#, reverse=True)
    dcm_array = np.stack([dcm.pixel_array for dcm in dcm_set])
    return dcm_array


def get_patient_segmentation(pid):
    seg_dir = base_dir() / "Train_Sets" / "CT" / f"{pid}" / "Ground"
    assert seg_dir.exists(), f"Patient does not have a ground truth mask:\n{seg_dir}"
    seg_fps = list(seg_dir.rglob("*.png"))
    assert len(seg_fps) == len(get_patient_dcm(pid)), f"The mask images do not match amount of dicom images:\n{seg_fps}"
    seg_fps.sort(key=lambda fp: int(fp.stem.split("_")[-1]))
    img_arrays = np.stack([np.array(Image.open(fp)) for fp in seg_fps], dtype=np.float32)
    return img_arrays


def get_patient_list():
    data_dir = base_dir() / "Train_Sets" / "CT"
    patients = sorted([pdir.name for pdir in data_dir.iterdir()])
    return patients


if __name__ == "__main__":
    patients = get_patient_list()
    for patient in patients:
        dcm = get_patient_dcm(patient)
        print(f"patient: {patient}: {len(dcm)}")
    print("done")