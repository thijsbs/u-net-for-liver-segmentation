import os
import torch
from torch.utils.data import Dataset, DataLoader
import numpy as np
from monai import transforms

import explore
import dcm_viewer

DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
# DEVICE = "cpu"

#Transforms to be applied on training instances
train_transform = transforms.Compose(
    [   
        # transforms.AddChanneld(keys=["image", "mask"]),
        transforms.Spacingd(keys=['image', 'mask'], pixdim=(1., 1., 1.), mode=("bilinear", "nearest")),
        transforms.ScaleIntensityRanged(keys=['image', 'mask'], a_min=0, a_max=2500, b_min=0, b_max=1000, clip=True),
        transforms.RandGaussianNoised(keys=['image'], prob=0.75, std=100),
        transforms.RandGaussianSmoothd(keys=["image"], prob=0.5),
        # transforms.RandScaleIntensityd(keys=['image'], factors=5, prob=1.0),
        # transforms.RandShiftIntensityd(keys=['image'], offsets=5, prob=1.0, safe=True),
        transforms.RandAffined(
            keys=['image', 'mask'], 
            prob=1.0,
            spatial_size=(256, 512, 512), 
            padding_mode="zeros",
            rotate_range=(np.pi / 4,),
            scale_range=(0.2, 0.5),
            translate_range=(100, 100),),
        # transforms.ResizeWithPadOrCropd(keys=["image", "mask"], spatial_size=(256, 512, 512)),
        transforms.ToTensord(keys=['image', 'mask'], device=DEVICE),
    ]
)

#Transforms to be applied on validation instances
val_transform = transforms.Compose(
    [   
        # transforms.AddChanneld(keys=["image", "mask"]),
        transforms.Spacingd(keys=['image', 'mask'], pixdim=(1., 1., 1.), mode=("bilinear", "nearest")),
        transforms.ScaleIntensityRanged(keys=['image', 'mask'], a_min=0, a_max=2500, b_min=0, b_max=1000, clip=True),
        transforms.ResizeWithPadOrCropd(keys=["image", "mask"], spatial_size=(256, 512, 512)),

        # transforms.ScaleIntensityRanged(keys=['image', 'mask'], a_min=0, a_max=2500, b_min=0, b_max=1000, clip=True),
        # transforms.NormalizeIntensityd(keys='image', nonzero=True, channel_wise=True),
        # transforms.DivisiblePadd(k=16, keys=["image", "mask"]),
        transforms.ToTensord(keys=['image', 'mask'], device=DEVICE),
    ]
)


class CTDataset(Dataset):
    def __init__(self, patients, transforms=None):

        self.patients = patients
        self.transforms = transforms

    def __len__(self):
        return len(self.patients)

    def __getitem__(self, index):
        # if torch.is_tensor(index):
            # index = index.tolist()
        print(type(index), index)
        patient = self.patients[index]
        dcm = explore.get_patient_dcm(patient)[None]
        mask = explore.get_patient_segmentation(patient)[None]
        data = {"image": dcm, "mask": mask}

        if self.transforms is not None:
            data = self.transforms(data)
        return data


if __name__ == "__main__":
    print(f"training on {DEVICE}")
    patients = explore.get_patient_list()

    dataset = CTDataset(patients, transforms=train_transform)

    train_dataloader = DataLoader(dataset=dataset, batch_size=3, shuffle=True)

    for data in train_dataloader:
        dcm = np.array(data["image"].cpu())
        mask = np.array(data["mask"].cpu())
        for batch_id in range(len(dcm)):
            viewer = dcm_viewer.DcmViewer()
            viewer.data = dcm[batch_id].squeeze()
            viewer.mask = mask[batch_id].squeeze()
            viewer.show()