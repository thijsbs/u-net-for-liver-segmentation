# U-Net for Liver Segmentation

A custom implementation of a 3D U-Net for liver segmentation using a very small dataset for training.

The dataset used comes from the *CHAOS - Combined (CT-MR) Healthy Abdominal Organ Segmentation* grand challenge.

The aim of this challenge is to create an automated organ segmentation system with a cross-modality input (CT and MRI).
I did not participate in the challenge, I am simply creating my own solution as a refresher for myself on creating and training a large segmentation model.

This project is currently work-in-progress. I am first focussing on training a model on CT data only and creating some tools to inspect the results. 
The next step is to train an MRI-only model, then a combined cross-modality model and compare the results.

thijs@biomedical-intelligence.com
