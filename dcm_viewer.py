"""A very simple dcm slice viewer"""
import numpy as np
import tkinter as tk
from tkinter import ttk
from PIL import ImageTk, ImageDraw, Image

import explore


class DcmViewer:

    def __init__(self, patient_list=None, ui_size=600):
        """Initialize.

        """
        # if not isinstance(dcm_set, np.ndarray):
        #     dcm_set = np.stack([dcm.pixel_array for dcm in dcm_set])
        if isinstance(ui_size, int):
            ui_size = (ui_size, ui_size)

        if patient_list is None: patient_list=[]
        self.ui_size = ui_size
        self.patient_list = patient_list
        self._data = None
        self._mask = None
        self._mouse_drag_start = (0, 0)
        self.window = 0
        self.level = 0
        self.slice_nr = 0
        self.create_window()


    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, dcm_set):
        """
        Args:
            dcm_set: A list of pydicom.DataSet objects or a numpy volume with
                slices stacked in the 1st dimension
        """
        assert isinstance(dcm_set, np.ndarray), f"Please provide a numpy array, not {type(dcm_set)}"
        self._data = dcm_set
        # print(f"min: {dcm_set.min()}, max: {dcm_set.max()}, mean: {dcm_set.mean()}, median: {np.median(dcm_set)}")
        self._mask = None
        self.window = self.data.max()/2
        self.level = self.data.mean()
        self.slice_nr = 0
        print("----------------")
        print(dcm_set.shape)
        print(len(dcm_set))
        print(dcm_set.max())
        print(dcm_set.mean())
        print("----------------")
        self.slice_selector.configure(to=len(dcm_set)-1)
        self.slice_selector.set(self.slice_nr)
        self.update_image()


    @property
    def mask(self):
        return self._mask

    @mask.setter
    def mask(self, mask_array):
        assert mask_array.shape == self.data.shape, "Mask data mismatch"
        self._mask = mask_array
        self.update_image()


    def create_window(self):
        self.root = tk.Tk()
        self.root.geometry(f"{self.ui_size[0]}x{self.ui_size[1]+120}")

        if self.patient_list:
            load_frame = tk.Frame(self.root)
            tk.Label(load_frame, text = "Patient: ").grid(row=0, column=0)
            self.current_patient = tk.StringVar()
            self.current_patient.set("None") 
            drop = tk.OptionMenu(load_frame, self.current_patient, *self.patient_list) 
            drop.grid(row=0, column=1)
            load_button = tk.Button(load_frame, text="load", command=self.load_patient)
            load_button.grid(row=0, column=3)
            load_frame.pack()

        img = self.get_slice_img(self.slice_nr)
        img = img.resize(self.ui_size)
        tkpi = ImageTk.PhotoImage(image=img)
        self.label_image = tk.Label(self.root, image=tkpi)
        self.label_image.pack()
        self.label_image.bind("<Button-3>", self.set_mouse_down_location)
        self.label_image.bind("<B3-Motion>", self.update_wl)
        self.label_image.bind("<Button-4>", self.scroll_up)
        self.label_image.bind("<Button-5>", self.scroll_down)

        self.slice_selector = tk.Scale(
            self.root, 
            from_=0, 
            to=0,
            resolution=1, 
            length=self.ui_size[0],
            orient=tk.HORIZONTAL, 
            showvalue=0,
            command=lambda i: self.set_slice_nr(i))
        self.slice_selector.set(self.slice_nr)
        self.slice_selector.pack()
          
    def show(self):
        self.root.mainloop()

    def load_patient(self):
        dcm = explore.get_patient_dcm(self.current_patient.get())
        mask = explore.get_patient_segmentation(self.current_patient.get())
        self.data = dcm
        self.mask = mask

    def set_slice_nr(self, nr):
        self.slice_nr = nr
        self.update_image()

    def set_mouse_down_location(self, event):
        self._mouse_drag_start = (event.x, event.y)

    def scroll_up(self, event):
        self.slice_selector.set(float(self.slice_nr)+1)

    def scroll_down(self, event):
        self.slice_selector.set(float(self.slice_nr)-1)


    def update_wl(self, event):
        self.window += (event.x - self._mouse_drag_start[0]) * (self.data.max()/(10*np.abs(self.data.mean())))
        self.window = max(self.window, 1)
        self.level -= (event.y - self._mouse_drag_start[1]) * (self.data.max()/(10*np.abs(self.data.mean())))
        self._mouse_drag_start = (event.x, event.y)
        self.update_image()


    def get_slice_img(self, slice_nr):
        if self.data is None:
            return Image.fromarray(np.zeros((512, 512), dtype=np.uint8))
        img = self.data[int(slice_nr)]
        maxval = self.level + 0.5*self.window
        minval = self.level - 0.5*self.window
        img = (img - self.level) / self.window
        img[img>=1] = 1
        img[img<=0] = 0
        img *= 255
        img = img[:, :, None].repeat(3, axis=2)
        return Image.fromarray(img.astype(np.uint8))

    def get_slice_mask(self, slice_nr):
        msk = self.mask[int(slice_nr)]
        msk_rgb = np.zeros(msk.shape + (4,))
        msk_rgb[:, :, 1] = msk
        msk_rgb[:, :, -1] = msk.astype(float) * 0.6
        msk_rgb *= 255
        mask_img = Image.fromarray(msk_rgb.astype(np.uint8))
        return mask_img

    def update_image(self):
        img = self.get_slice_img(self.slice_nr)
        img = img.resize(self.ui_size)
        if self.mask is not None:
            msk = self.get_slice_mask(self.slice_nr)
            msk = msk.resize(self.ui_size)
            img.paste(msk, (0,0), msk)
            # img = msk

        tkpi = ImageTk.PhotoImage(image=img)
        self.label_image.configure(image=tkpi)
        self.label_image.image = tkpi


if __name__ == "__main__":

    pid = 22
    # dcm = explore.get_patient_dcm(pid)
    # mask = explore.get_patient_segmentation(pid)
    # viewer = DcmViewer()
    viewer = DcmViewer(explore.get_patient_list())
    # viewer.add_mask(mask)
    # viewer.update_image()
    viewer.show()